<?php

/**
 * @file
 * Chosen administration pages.
 */

/**
 * Returns with the general configuration form.
 */
function responsive_bootstrap_block_settings($form, &$form_state) {

  $form['responsive_bootstrap_block_load_css'] = array(
    '#type' => 'checkbox',
    '#title' => t('Responsive Stylesheet'),
    '#default_value' => variable_get('responsive_bootstrap_block_load_css', 'none'),
    '#description' => t('Include Bootstrap Responsive Stylesheet (Column Size, Offset, Push, Pull and Toggle.'),
  );

  return system_settings_form($form);
}
