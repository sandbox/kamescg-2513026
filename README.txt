Responsive Bootstrap Block
====================================
The Responsive Block Modules extends Drupal Blocks with responsive features
(Column Size, Offset, Push, Pull and Toggle)

DESCRIPTION
-----------
The Responsive Block Module extends Blocks with the Bootstraps Column and Toggle
settings, so administrators can change a block's responsive features,without
having to edit the CSS.
